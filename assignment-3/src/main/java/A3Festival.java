import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import javari.animal.Animal;
import javari.animal.phylum.Aves;
import javari.animal.phylum.Mammal;
import javari.animal.phylum.Reptile;
import javari.park.Attraction;
import javari.park.Section;
import javari.park.SelectedAttraction;
import javari.park.Visitor;
import javari.reader.AnimalReader;
import javari.reader.AttractionReader;
import javari.reader.CategoryReader;
import javari.reader.SectionReader;
import javari.writer.RegistrationWriter;

/**
 * This class describes the Festival on Javari Park.
 *
 * @author Jonathan Christopher Jakub.
 */
public class A3Festival {
    private static String animalCTG = "animals_categories.csv";
    private static String animalATR = "animals_attractions.csv";
    private static String animalREC = "animals_records.csv";

    /**
     * Class : Main.
     * defined main() in this class
     * @author  Jonathan Christopher Jakub.
     * @since   2-4-2018.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<Visitor> visitors = new ArrayList<Visitor>();
        CategoryReader ctgr;
        SectionReader sect;
        AttractionReader attr;
        AnimalReader rcrd;
        System.out.println("Welcome  to Javari Park Festival - Registration Service!\n");
        
        //Read from the data
        String inputPath = "";
        System.out.println("...Opening default section database from data. ...");
        while (true) {
            try {
                sect = new SectionReader(Paths.get(inputPath, animalCTG));
                attr = new AttractionReader(Paths.get(inputPath, animalATR));
                ctgr = new CategoryReader(Paths.get(inputPath, animalCTG));
                rcrd = new AnimalReader(Paths.get(inputPath, animalREC));
                break;
            } catch (IOException e) {
                System.out.println("File not found or incorrect file!");
                System.out.print("Please provide the source data path: ");
                inputPath = in.nextLine();
            }
        }
        System.out.println("... Loading... Success... System is populating data...\n");
        System.out.println("Found " + sect.countValidRecords() 
            + " valid sections and " + sect.countInvalidRecords() 
            + " invalid sections");
        System.out.println("Found " + attr.countValidRecords() 
            + " valid attractions and " + attr.countInvalidRecords() 
            + " invalid attractions");
        System.out.println("Found " + ctgr.countValidRecords() 
            + " valid animal categories and " + ctgr.countInvalidRecords() 
            + " invalid animal categories");
        System.out.println("Found " + rcrd.countValidRecords() 
            + " valid animal records and " + rcrd.countInvalidRecords() 
            + " invalid animal records");
        
        //Populate data
        ArrayList<Section> sections = populateSection(sect);
        ArrayList<Attraction> attractions = populateAttraction(attr);
        ArrayList<Animal> animals = populateAnimal(rcrd);
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");

        //Option menus
        Section chosenSection = null;
        Attraction chosenAttraction = null;
        String chosenType = null;
        Visitor issuedVisitor = null;
        String visitorName = null;
        int currentState = 1;
        while (true) {
            System.out.println("Please answer the questions by typing the number. "
                + "Type # if you want to return to the previous menu");
            while (true) {
                //Section menu
                if (currentState == 1) {
                    chosenSection = chooseSection(sections);
                    if (chosenSection == null) {
                        currentState = 1;
                        continue;
                    } else {
                        currentState = 2;
                        System.out.println("\n--" + chosenSection.getName() + "--");
                    }
                }
                //Type menu
                if (currentState == 2) {
                    chosenType = chooseType(chosenSection);
                    if (chosenType == null) {
                        currentState = 1;
                        continue;
                    } else {
                        currentState = 3;
                        System.out.println("\n--" + chosenType + "--");
                        System.out.println("Attractions by " + chosenType);
                    }
                }
                //Attractioin menu
                if (currentState == 3) {
                    chosenAttraction = chooseAttraction(attractions, chosenType, animals);
                    if (chosenAttraction == null) {
                        currentState = 2;
                        continue;
                    } else if(chosenAttraction.getPerformers().size() == 0) {
                        System.out.println("\nUnfortunately, no " + chosenType.toLowerCase() 
                            + " can perform any attraction, please choose other animals");
                        currentState = 2;
                    } else {
                        break;
                    }
                }
            }
            //Registrate visitor
            if (issuedVisitor == null && visitorName == null) {
                System.out.println("\nWow, one more step,");
                System.out.print("please let us know your name: ");
                String path = "registration";
                while (true) {
                    try {
                        issuedVisitor = new Visitor(path);
                        break;
                    } catch (IOException e) {
                        System.out.print("Default folder: \"registration\""
                            + "not found! Please provide another folder :");
                        path = in.nextLine();
                    }
                }
                visitorName = in.nextLine();
                issuedVisitor.setVisitorName(visitorName);
            }
            if (!issuedVisitor.getSelectedAttractions().contains(chosenAttraction)) {
                issuedVisitor.addSelectedAttraction(chosenAttraction);
            }
            //Print information of the visitor
            System.out.println("\nYeay, final check!");
            System.out.println("Here is your data, and the attraction you choose:");
            System.out.println("Name: " + issuedVisitor.getVisitorName());
            for (SelectedAttraction a: issuedVisitor.getSelectedAttractions()) {
                System.out.print("Attraction: " + a.getName());
                System.out.println(" -> " + a.getType());
                String performers = "";
                for (Animal p: a.getPerformers()) {
                    performers += p.getName() + ", ";
                }
                System.out.println("With: " + performers.substring(0, performers.length() - 2));
            }
            System.out.print("\nIs the data correct? (Y/N): ");
            if (in.next().equalsIgnoreCase("N")) {
                System.out.println("Sorry, let's try again :(");
                currentState = 1;
                continue;
            }
            //Write to JSON
            System.out.print("Thank you for your interest. "
                + "Would you like to register to other attractions? (Y/N): ");
            if (in.next().equalsIgnoreCase("N")) {
                Path regPath = Paths.get("registration/");
                while (true) {
                    try {
                        RegistrationWriter.writeJson(issuedVisitor, regPath);
                        break;
                    } catch (IOException e) {
                        System.out.print("Default path not found, "
                            + "Please provide the source data path: ");
                        String newPath = in.nextLine();
                        regPath = Paths.get(newPath);
                    }
                }
                break;
            } else {
                currentState = 1;
            }
        }
    }

    /**
     * Static helper method to fill the Park with sections.
     * 
     * @param sect as List of all sections that will be in the park.
     */
    private static ArrayList<Section> populateSection(SectionReader sect) {
        ArrayList<Section> sections = new ArrayList<Section>();
        for (String line: sect.getValidLines()) {
            String[] entry = line.split(sect.COMMA);
            boolean found = false;
            for (Section s: sections) {
                if (s.getName().equals(entry[2])) {
                    s.addType(entry[0]);
                    found = true;
                    break;
                }
            }
            if (found == false) {
                Section newSection = new Section(entry[2]);
                newSection.addType(entry[0]);
                sections.add(newSection);
            }
        }
        return sections;
    }

    /**
     * Static helper method to fill the Park with attractions.
     * 
     * @param attr as List of all attractions that will be in the park.
     */
    private static ArrayList<Attraction> populateAttraction(AttractionReader attr) {
        ArrayList<Attraction> attractions = new ArrayList<Attraction>();
        for (String line: attr.getValidLines()) {
            String[] entry = line.split(attr.COMMA);
            attractions.add(new Attraction(entry[0], entry[1]));
        }
        return attractions;
    }

    /**
     * Static helper method to fill the Park with animals.
     * 
     * @param rcrd as List of all animals that will be in the park.
     */
    private static ArrayList<Animal> populateAnimal(AnimalReader rcrd) {
        ArrayList<Animal> animals = new ArrayList<Animal>();
        for (String line: rcrd.getValidLines()) {
            String[] entry = line.split(rcrd.COMMA);
            if (Arrays.asList("Hamster", "Lion", "Cat", "Whale").contains(entry[1])) {
                animals.add((Animal) new Mammal(
                    entry[0], 
                    entry[1], 
                    entry[2], 
                    entry[3], 
                    entry[4], 
                    entry[5], 
                    entry[6], 
                    entry[7]
                ));
            } else if (Arrays.asList("Parrot", "Eagle").contains(entry[1])) {
                animals.add((Animal) new Aves(
                    entry[0], 
                    entry[1], 
                    entry[2], 
                    entry[3], 
                    entry[4], 
                    entry[5], 
                    entry[6], 
                    entry[7]
                ));
            } else if (Arrays.asList("Snake").contains(entry[1])) {
                animals.add((Animal) new Reptile(
                    entry[0], 
                    entry[1], 
                    entry[2], 
                    entry[3], 
                    entry[4], 
                    entry[5], 
                    entry[6], 
                    entry[7]
                ));
            }
        }
        return animals;
    }

    /**
     * Static helper method to simulate menu behavior for choosing section to visit.
     * 
     * @param sections as List of all sections in the park.
     */
    private static Section chooseSection(ArrayList<Section> sections) {
        Scanner in = new Scanner(System.in);
        Section chosenSection = null;
        System.out.println("\nJavari Park has 3 sections:");
        HashMap<Integer, Section> choiceSection = new HashMap<>();
        for (int i = 0; i < sections.size(); i++) {
            choiceSection.put(i + 1, sections.get(i));
            System.out.println((i + 1) + ". " + choiceSection.get(i + 1).getName());
        }
        System.out.println("Please choice your preferred section (type the number): ");
        try {
            int choice1 = in.nextInt();
            if (choiceSection.containsKey(choice1)) {
                chosenSection = choiceSection.get(choice1);
            }
            return chosenSection;
        } catch (InputMismatchException | NullPointerException e) {
            return null;
        }
    }

    /**
     * Static helper method to simulate menu behavior for choosing type of animal
     *  to visit in chosen section.
     * 
     * @param chosenSection the selected section visitor wants to visit.
     */
    private static String chooseType(Section chosenSection) {
        Scanner in = new Scanner(System.in);
        String chosenType = "";
        HashMap<Integer, String> choiceType = new HashMap<>();
        for (int i = 0; i < chosenSection.getTypes().size(); i++) {
            choiceType.put(i + 1, chosenSection.getTypes().get(i));
            System.out.println((i + 1) + ". " + choiceType.get(i + 1));
        }
        System.out.println("Please choice your preferred animals (type the number): ");
        try {
            int choice2 = in.nextInt();
            if (choiceType.containsKey(choice2)) {
                chosenType = choiceType.get(choice2);
            }
            return chosenType;
        } catch (InputMismatchException | NullPointerException e) {
            return null;
        }
    }

    /**
     * Static helper method to simulate menu behavior for choosing 
     *  the attraction.
     * 
     * @param attractions as the list of all attractions in the park.
     * @param chosenType the type of animal visitor wants to see in the attraction.
     * @param animals as the list of all the animals in the park.
     */
    private static Attraction chooseAttraction(
        ArrayList<Attraction> attractions, 
        String chosenType, 
        ArrayList<Animal> animals) {
        Scanner in = new Scanner(System.in);
        Attraction chosenAttraction = null;
        HashMap<Integer, Attraction> choiceAttraction = new HashMap<>();
        int counter = 0;
        for (int i = 0; i < attractions.size(); i++) {
            if (attractions.get(i).getType().equals(chosenType)) {
                counter += 1;
                choiceAttraction.put(counter, attractions.get(i));
                System.out.println(counter + ". " + choiceAttraction.get(counter).getName());
            }
        }
        System.out.println("Please choice your preferred attractions (type the number): ");
        try {
            int choice3 = in.nextInt();
            if (choiceAttraction.containsKey(choice3)) {
                chosenAttraction = choiceAttraction.get(choice3);
                for (Animal p: animals) {
                    if (p.getType().equals(chosenType)) {
                        chosenAttraction.addPerformer(p);
                    }
                }
            }
            return chosenAttraction;
        } catch (InputMismatchException | NullPointerException e) {
            return null;
        }
    }
}