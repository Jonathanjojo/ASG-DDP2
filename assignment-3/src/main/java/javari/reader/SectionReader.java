package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javari.reader.CsvReader;


public class SectionReader extends CsvReader {
    HashMap<String,List<String>> validCategory = new HashMap<>();
    HashMap<String, String> validSection = new HashMap<>();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public SectionReader(Path file) throws IOException {
        super(file);
        validSection.put("mammals", "Explore the Mammals");
        validSection.put("reptiles", "Reptillian Kingdom");
        validSection.put("aves", "World of Aves");

        validCategory.put("mammals", Arrays.asList(
            "Cat",
            "Lion",
            "Whale",
            "Hamster"
        ));
        validCategory.put("reptiles", Arrays.asList(
            "Snake"
        ));
        validCategory.put("aves", Arrays.asList(
            "Eagle",
            "Parrot"
        ));
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return int number of valid records.
     */
    public long countValidRecords() {
        HashSet<String> sections = new HashSet<String>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (isValid(entry)) {
                sections.add(entry[2]);
            } else {
                if (sections.contains(entry[2])) {
                    sections.remove(entry[2]);
                }
            }

        }
        return sections.size();
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return int number of invalid records.
     */
    public long countInvalidRecords() {
        HashSet<String> sections = new HashSet<String>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (isValid(entry)) {
                sections.add(entry[2]);
            }

        }
        return sections.size();
    }

    /**
     * Helper method to determine a record is valid.
     *
     * @return boolean true if entry is valid, false otherwise.
     */
    private boolean isValid(String[] entry) {
        try {
            if ((validSection.get(entry[1]).equals(entry[2])) 
                && (validCategory.get(entry[1]).contains(entry[0]))) {
                return true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

    /**
     * Returns all valid lines of text from CSV file as a list.
     *
     * @return list of valid lines.
     */
    public ArrayList<String> getValidLines() {
        ArrayList<String> out = new ArrayList<String>();
        for (String s : getLines()) {
            if (isValid(s.split(super.COMMA))) {
                out.add(s);
            }
        }
        return out;
    }
}