package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javari.reader.CsvReader;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Jonathan Christopher Jakub.
 */
public class AnimalReader extends CsvReader {
    private List<String> genders = Arrays.asList("male", "female");
    private List<String> health = Arrays.asList("healthy", "not healthy");
    private List<String> types = Arrays.asList("Eagle", "Cat", "Lion", "Parrot", 
        "Snake", "Hamster", "Whale");

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return int number of valid records.
     */
    public long countValidRecords() {
        HashSet<String[]> datas = new HashSet<String[]>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (isValid(entry)) {
                datas.add(entry);
            } else {
                if (datas.contains(entry)) {
                    datas.remove(entry);
                }
            }

        }
        return datas.size();
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return int number of invalid records.
     */
    public long countInvalidRecords() {
        HashSet<String[]> datas = new HashSet<String[]>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (!isValid(entry)) {
                datas.add(entry);
            }

        }
        return datas.size();
    }

    /**
     * Helper method to determine whether a data is numeric.
     *
     * @return boolean true if String s is numeric, false otherwise.
     */
    private boolean isNumeric(String s) {
        try {
            Double num = Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Helper method to determine a record is valid.
     *
     * @return boolean true if entry is valid, false otherwise.
     */
    private boolean isValid(String[] entry) {
        try {
            if ((isNumeric(entry[0])) 
                && (types.contains(entry[1])) 
                && (!entry[2].equals("")) 
                && (genders.contains(entry[3])) 
                && (isNumeric(entry[4])) 
                && (isNumeric(entry[5])) 
                && (health.contains(entry[7]))) {
                return true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

    /**
     * Returns all valid lines of text from CSV file as a list.
     *
     * @return list of valid lines.
     */
    public ArrayList<String> getValidLines() {
        ArrayList<String> out = new ArrayList<String>();
        for (String s : getLines()) {
            if (isValid(s.split(super.COMMA))) {
                out.add(s);
            }
        }
        return out;
    }
}