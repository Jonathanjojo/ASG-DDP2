package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javari.reader.CsvReader;

public class AttractionReader extends CsvReader {
    HashMap<String,List<String>> validAttraction = new HashMap<>();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AttractionReader(Path file) throws IOException {
        super(file);
        validAttraction.put("Dancing Animals", Arrays.asList(
            "Cat",
            "Snake",
            "Parrot",
            "Hamster"
        ));
        validAttraction.put("Circles of Fires", Arrays.asList(
            "Whale",
            "Lion",
            "Eagle"
        ));
        validAttraction.put("Counting Masters", Arrays.asList(
            "Hamster",
            "Whale",
            "Parrot"
        ));
        validAttraction.put("Passionate Coders", Arrays.asList(
            "Cat",
            "Hamster",
            "Snake"
        ));
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return int number of valid records.
     */
    public long countValidRecords() {
        HashSet<String> attractions = new HashSet<String>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (isValid(entry)) {
                attractions.add(entry[1]);
            } else {
                if (attractions.contains(entry[1])) {
                    attractions.remove(entry[1]);
                }
            }

        }
        return attractions.size();
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return int number of invalid records.
     */
    public long countInvalidRecords() {
        HashSet<String> attractions = new HashSet<String>();
        for (String records : super.getLines()) {
            String[] entry = records.split(super.COMMA);
            if (!isValid(entry)) {
                attractions.add(entry[1]);
            }

        }
        return attractions.size();
    }

    /**
     * Helper method to determine a record is valid.
     *
     * @return boolean true if entry is valid, false otherwise.
     */
    private boolean isValid(String[] entry) {
        try {
            if (validAttraction.get(entry[1]).contains(entry[0])) {
                return true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

    /**
     * Returns all valid lines of text from CSV file as a list.
     *
     * @return list of valid lines.
     */
    public ArrayList<String> getValidLines() {
        ArrayList<String> out = new ArrayList<String>();
        for (String s : getLines()) {
            if (isValid(s.split(super.COMMA))) {
                out.add(s);
            }
        }
        return out;
    }

}