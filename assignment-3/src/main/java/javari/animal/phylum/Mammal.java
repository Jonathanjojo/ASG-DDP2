package javari.animal.phylum;

import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public class Mammal extends Animal {
    private boolean pregnant;

    /**
     * Constructs an instance of Mammal.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param strPregnant    boolean state of whether animal is pregnant
     * @param condition     health condition of the animal
     */
    public Mammal(String id, String type, String name, String gender, String length,
        String weight, String strPregnant, String condition) {
        super(
            Integer.parseInt(id), 
            type, 
            name, 
            Gender.parseGender(gender), 
            Double.parseDouble(length), 
            Double.parseDouble(weight), 
            Condition.parseCondition(condition)
        );
        if (strPregnant.equals("pregnant")) {
            this.pregnant = true;
        } else {
            this.pregnant = false;
        }
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return true if able to perform, false otherwise.
     */
    protected boolean specificCondition() {
        if (this.getType().equals("Lion") && this.getGender() == Gender.MALE) {
            return true;
        }
        return !pregnant;
    }
}