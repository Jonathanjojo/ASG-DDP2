package javari.animal.phylum;

import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public class Aves extends Animal {
    private boolean layingEggs;

    /**
     * Constructs an instance of Aves.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param strLayingEggs  boolean state of whether animal is laying eggs
     * @param condition     health condition of the animal
     */
    public Aves(String id, String type, String name, String gender, String length,
        String weight, String strLayingEggs, String condition) {
        super(
            Integer.parseInt(id), 
            type, 
            name, 
            Gender.parseGender(gender), 
            Double.parseDouble(length), 
            Double.parseDouble(weight), 
            Condition.parseCondition(condition)
        );
        if (strLayingEggs.equals("laying eggs")) {
            this.layingEggs = true;
        } else {
            this.layingEggs = false;
        }
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return true if able to perform, false otherwise.
     */
    protected boolean specificCondition() {
        return !layingEggs;
    }
}