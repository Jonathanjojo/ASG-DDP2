package javari.animal.phylum;

import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public class Reptile extends Animal {
    private boolean tamed;

    /**
     * Constructs an instance of Reptile.
     *
     * @param id            unique identifier
     * @param type          type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name          name of animal, e.g. hamtaro, simba
     * @param gender        gender of animal (male/female)
     * @param length        length of animal in centimeters
     * @param weight        weight of animal in kilograms
     * @param strTamed      boolean state of whether animal is tamed
     * @param condition     health condition of the animal
     */
    public Reptile(String id, String type, String name, String gender, String length,
        String weight, String strTamed, String condition) {
        super(
            Integer.parseInt(id), 
            type, 
            name, 
            Gender.parseGender(gender), 
            Double.parseDouble(length), 
            Double.parseDouble(weight), 
            Condition.parseCondition(condition)
        );
        if (strTamed.equals("wild")) {
            this.tamed = false;
        } else {
            this.tamed = true;
        }
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return true if able to perform, false otherwise.
     */
    protected boolean specificCondition() {
        return tamed;
    }
}