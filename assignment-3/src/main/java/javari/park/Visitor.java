package javari.park;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class describes attraction registration
 * done by a visitor in Javari Park.
 *
 * @author Jonathan Christopher Jakub.
 */
public class Visitor implements Registration {
    int id;
    String name;
    ArrayList<SelectedAttraction> attraction = new ArrayList<SelectedAttraction>();

    /**
     * Defines the base constructor for instantiating an object of Visitor.
     */
    public Visitor(String path) throws IOException {
        this.id = new File(path).listFiles().length + 1;
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return int ID.
     */
    public int getRegistrationId() {
        return this.id;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return String name of visitor.
     */
    public String getVisitorName() {
        return this.name;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return String name of the visitor.
     */
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return List of selected attractions.
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attraction;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return true if the attraction is successfully added into the
     *     list, false otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.attraction.add(selected);
        return true;
    }
}