package javari.park;

import java.util.ArrayList;

public class Section {
    private String name;
    private ArrayList<String> types = new ArrayList<String>();

    /**
     * Defines the Section containing certain types of animals.
     * @param name  name of the section.
     */
    public Section(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the section.
     *
     * @return String name of section.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the types of animal contained within the section.
     *
     * @return String type of contained animals.
     */
    public ArrayList<String> getTypes() {
        return this.types;
    }

    /**
     * Add new type to the section.
     * 
     * @param type  add type of animal to the section.
     */
    public void addType(String type) {
        this.types.add(type);
    }
}