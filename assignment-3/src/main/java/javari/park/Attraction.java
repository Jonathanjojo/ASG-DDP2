package javari.park;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javari.animal.Animal;
import javari.park.SelectedAttraction;

/**
 * This interface describes expected behaviours for any type
 * (class, interface) that represents the concept of attraction that
 * will be watched by a visitor in Javari Park.
 *
 * @author Jonathan Christopher Jakub.
 */
public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private ArrayList<Animal> performers = new ArrayList<Animal>();
    private HashMap<String,List<String>> validPerformers = new HashMap<String, List<String>>();

    /**
     * Defines the base constructor for instantiating an object of Attraction.
     *
     * @param type  type of the animals performing the attraction
     * @param name  name of the attraction
     */
    public Attraction(String type, String name) {
        this.name = name;
        this.type = type;

        validPerformers.put("Dancing Animals", Arrays.asList(
            "Cat",
            "Snake",
            "Parrot",
            "Hamster"
        ));

        validPerformers.put("Circles of Fires", Arrays.asList(
            "Whale",
            "Lion",
            "Eagle"
        ));
    
        validPerformers.put("Counting Masters", Arrays.asList(
            "Hamster",
            "Whale",
            "Parrot"
        ));
        
        validPerformers.put("Passionate Coders", Arrays.asList(
            "Cat",
            "Hamster",
            "Snake"
        ));
    }

    /**
     * Returns the name of attraction.
     *
     * @return String name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return String type.
     */
    public String getType() {
        return this.type;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return list of performers.
     */
    public ArrayList<Animal> getPerformers() {
        return this.performers;
    }

    /**
     * Returns the map of allowed type of animals to perform.
     *
     * @return HashMap.
     */
    public HashMap<String, List<String>> getValidPerformers() {
        return this.validPerformers;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return true if the animal is successfully added into list of
     *     performers, false otherwise
     */
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable() 
            && this.getValidPerformers().get(this.name).contains(performer.getType())) {
            this.getPerformers().add(performer);
            return true;
        }
        return false;
    }
}