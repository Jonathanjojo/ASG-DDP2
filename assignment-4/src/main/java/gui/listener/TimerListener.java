package gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

import gui.view.GamePanel;

/**
 * Action Listener of the timer.
 * This listener will be called if cards are pressed and still unmatched.
 * This listener will show the card for 0.75 secs and then
 * proceed to close it if the cards are not matched by the press.
 * 
 * @author Jonathan Christopher Jakub
 */
public class TimerListener implements ActionListener {

    /**
     * The game panel accessing this timer listener.
     */
    private GamePanel gamePanel;

    /**
     * Constructor of the listener.
     * 
     * @param gamePanel the game panel with memory of the first and second card clicked. 
     */
    public TimerListener(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    /**
     * Listener handler to process any click event.
     * 
     * @param e the click event.
     */
    public void actionPerformed(ActionEvent e) {
        try {
            this.gamePanel.getFirstCard().close();
            this.gamePanel.getSecondCard().close();
        } catch (NullPointerException np) {
            ;
        }
        this.gamePanel.setFirstCard(null);
        ((Timer)e.getSource()).stop();
    }

}