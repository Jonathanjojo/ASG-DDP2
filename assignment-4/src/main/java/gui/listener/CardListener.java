package gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import gui.view.GamePanel;
import gui.util.Card;

/**
 * Listener of the card buttons.
 * This listener will handle click events of the cards.
 * 
 * @author Jonathan Christopher Jakub
 */
public class CardListener implements ActionListener {

    /**
     * The game panel accessing this card listener.
     */
    private GamePanel gamePanel;

    /**
     * Constructor of the listener.
     * 
     * @param gamePanel the game panel with memory of the first and second card clicked, and the timer.
     */
    public CardListener(GamePanel gamePanel){
        this.gamePanel = gamePanel;
    }

    /**
     * Listener handler that will process any click event
     * to the card buttons.
     * 
     * @param e the click event.
     */
    public void actionPerformed(ActionEvent e) {
        if (!this.gamePanel.getTimer().isRunning() && !(((Card)e.getSource()).isMatched())) {
            Card current = (Card)e.getSource();
            current.open();

            if (this.gamePanel.getFirstCard() == null) {
                this.gamePanel.setFirstCard(current);
            } else {
                this.gamePanel.setSecondCard(current);
            }
            
            if ((this.gamePanel.getFirstCard() != null && this.gamePanel.getSecondCard() != null))  {
                if (this.gamePanel.getFirstCard() != current) {
                    if (this.gamePanel.getFirstCard().getContent() != this.gamePanel.getSecondCard().getContent()) {
                        this.gamePanel.getTimer().start(); 
                    } else {
                        this.gamePanel.getFirstCard().match();
                        this.gamePanel.getSecondCard().match();
                        this.gamePanel.getFirstCard().dissapear();
                        this.gamePanel.getSecondCard().dissapear();
                        this.gamePanel.setFirstCard(null);
                        if(this.gamePanel.checkWin()){
                            JOptionPane.showMessageDialog(this.gamePanel, "Congratulations! You win!\nTrials: " 
                                + this.gamePanel.getTrial());
                            this.gamePanel.resetBoard();
                            this.gamePanel.resetTrial();

                        }
                    }
                    this.gamePanel.addTrial();
                }
            }
        }
    }
}