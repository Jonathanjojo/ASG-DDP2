package gui.util;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 * This class represents the cards that are played.
 * 
 * @author Jonathan Christopher Jakub
 */
public class Card extends JButton {

    /**
     * The card back image.
     */
    private ImageIcon cardBack;

    /**
     * The card actual image.
     */
    private ImageIcon actualCard;

    /**
     * State of the cards match.
     */
    private boolean matched;

    /**
     * Content of the card.
     */
    private String content;

    /**
     * Constructor of card instance.
     * 
     * @param cardBackURL string path of the card back image.
     * @param actualCardURL string path of the card image.
     */
    public Card(String cardBackURL, String actualCardURL) {
        super();
        this.setSize(75, 75);
        this.cardBack = new ImageIcon(this.scaleImage(cardBackURL));
        this.actualCard = new ImageIcon(this.scaleImage(actualCardURL));
        this.setMargin(new Insets(0, 0, 0, 0));
        this.matched = false;
        this.content = actualCardURL;
    }

    /**
     * Accessor of the content of the card.
     * The content of the card is the path of the image.
     * 
     * @return string content.
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Close the card to show the card back.
     * 
     */
    public void close() {
        this.setIcon(this.cardBack);
    }

    /**
     * Open the card to show the card true image.
     */
    public void open() {
        this.setIcon(this.actualCard);
    }

    /**
     * Set this card as matched with other card.
     */
    public void match() {
        this.matched = true;
    }

    /**
     * Accessor of match status of this card.
     * 
     * @return true if this card is matched, false otherwise.
     */
    public boolean isMatched() {
        return this.matched;
    }

    /**
     * Reset this card to play another round.
     * This card's content and image are updated.
     * 
     * @param newActualCard string path of the new image.
     */
    public void reset(String newActualCard) {
        this.setSize(75, 75);
        this.setMargin(new Insets(0, 0, 0, 0));
        this.matched = false;
        this.close();
        this.actualCard = new ImageIcon(this.scaleImage(newActualCard));
        this.content = newActualCard;
    }

    /**
     * Hide the card if the card is matched.
     */
    public void dissapear() {
        this.setOpaque(false);
        this.setIcon(null);
        this.setContentAreaFilled(false);
    }

    /**
     * Helper method to scale the image to fit
     * to the size of the button.
     * 
     * @param path string path of the image.
     */
    private Image scaleImage(String path) {
        try {
            BufferedImage scaledImage = ImageIO.read(new File(path));
            Image newScaled = scaledImage.getScaledInstance(this.getSize().width, this.getSize().height,
                    java.awt.Image.SCALE_SMOOTH);
            return newScaled;
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(this, "File Not Found : Cards' images not found");
            System.exit(0);
            return null;
        }
    }
}