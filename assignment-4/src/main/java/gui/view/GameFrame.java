package gui.view;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * This class represent the frame of the
 * Memory Game GUI.
 * 
 * @author Jonathan Christopher Jakub
 */
public class GameFrame extends JFrame{

    /**
     * Name of the frame.
     */
    private static final String name = "TP4 - Memory Game";

    /**
     * BG color of the frame.
     */
    private static final Color bgColor = Color.GRAY;

    /**
     * Constructor of the frame.
     * 
     * @param imagePath the path to the folder containing images for the cards.
     */
    public GameFrame(String imagePath) {
        this.setTitle(this.name);
        this.setBackground(this.bgColor);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_VERT); 
        this.setVisible(true);
        this.setResizable(false);
        try {
            this.setIconImage((new ImageIcon(imagePath + "logo.png")).getImage());
        } catch (NullPointerException np) {
            //use default image
        }
        this.add(new GamePanel(imagePath));
        this.pack();
    }
}