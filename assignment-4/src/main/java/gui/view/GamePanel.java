package gui.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import gui.listener.TimerListener;
import gui.util.Card;
import gui.view.pane.Board;
import gui.view.pane.Controller;

/**
 * This class represents the game panel of the memory game frame.
 */
public class GamePanel extends JPanel{

    /**
     * Timer of the game with delay set 750 ms.
     */
    private final Timer timer = new Timer(750, new TimerListener(this));

    /**
     * Variable containing the first card clicked.
     */
    private Card firstCard;

    /**
     * Variable containing the second card clicked.
     */
    private Card secondCard;

    /**
     * The board game of the game.
     */
    private Board board;

    /**
     * The panel of the buttons and the label of the game.
     */
    private Controller controller;

    /**
     * Constructor of the game panel.
     * 
     * @param imagePath string path of the folder containing images for the cards.
     */
    public GamePanel(String imagePath) {
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        gbc.gridx = 1;
        gbc.gridy = 0;
        
        this.board = new Board(this, imagePath);
        this.add(this.board, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1; 
        this.controller = new Controller(this);
        this.add(this.controller, gbc);
    }

    /**
     * Accessor for the first card clicked.
     * Any card clicked after the second card
     * will be treated as the first card.
     * 
     * @return the first card clicked.
     */
    public Card getFirstCard(){
        return this.firstCard;
    }

    /**
     * Mutator for the first card.
     * 
     * @param another card clicked as the first card.
     */
    public void setFirstCard(Card another){
        this.firstCard = another;
    }

    /**
     * Accessor for the second card clicked.
     * 
     * @return the second card clicked.
     */
    public Card getSecondCard(){
        return this.secondCard;
    }

    /**
     * Mutator for the second card.
     * 
     * @param another card clicked as the second card.
     */
    public void setSecondCard(Card another) {
        this.secondCard  = another;
    }

    /**
     * Accessor of the timer.
     * 
     * @return timer used for the game.
     */
    public Timer getTimer(){
        return this.timer;
    }

    /**
     * Helper method to check if the player already wins.
     * 
     * @return boolean true if all cards are matched, false otherwise.
     */
    public boolean checkWin(){
        return this.board.checkWin();
    }

    /**
     * Add trial counter.
     */
    public void addTrial(){
        this.controller.addTrial();
    }

    /**
     * Reset trial counter.
     */
    public void resetTrial() {
        this.controller.resetTrial();
    }

    /**
     * Accessor for trial counter.
     * 
     * @return trial counter.
     */
    public int getTrial() {
        return this.controller.getTrial();
    }

    /**
     * Reset the board
     * and reset the first and second card.
     */
    public void resetBoard(){
        this.firstCard = null;
        this.secondCard = null;
        this.board.reset();
        this.timer.start();
    }

}