package gui.view.pane;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.view.GamePanel;

/**
 * This class represents the panel of the buttons and label of
 * the memory game.
 * 
 * @author Jonathan Christopher Jakub
 */
public class Controller extends JPanel {

    /**
     * The game panel containing this controller.
     */
    private GamePanel gamePanel;

    /**
     * The replay button to reset the game.
     */
    private JButton replayButton;

    /**
     * The exit button to quit the game.
     */
    private JButton exitButton;

    /**
     * The trial label containing information of trials done by player during the game.
     */
    private JLabel trialLabel;

    /**
     * The trial counter.
     */
    private int trials = 0;

    /**
     * Constructor for the controller panel.
     * 
     * @param gamePanel that contains this panel with the information
     * of the board and the trials.
     */
    public Controller(GamePanel gamePanel){
        this.gamePanel = gamePanel;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.replayButton = new JButton("replay");
        this.replayButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                gamePanel.resetBoard();
                gamePanel.resetTrial();
            }
        });
        buttonPanel.add(replayButton);

        this.exitButton = new JButton("exit");
        this.exitButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        buttonPanel.add(exitButton);

        buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(buttonPanel, this);

        this.trialLabel = new JLabel("Number of trials: " + trials);
        this.trialLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(trialLabel, this);
    }

    /**
     * Accessor for trial counter.
     * 
     * @return trial counter.
     */
    public int getTrial() {
        return this.trials;
    }

    /**
     * Increment the counter of the trial
     * and update the trial label.
     */
    public void addTrial(){
        this.trialLabel.setText("Number of trials: " + ++this.trials);
    }

    /**
     * Reset the trial counter
     * and update the trial label.
     */
    public void resetTrial(){
        this.trials = 0;
        this.trialLabel.setText("Number of trials: " + this.trials);
    }
}
