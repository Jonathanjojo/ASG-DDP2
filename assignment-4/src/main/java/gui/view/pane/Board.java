package gui.view.pane;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

import gui.listener.CardListener;
import gui.util.Card;
import gui.view.GamePanel;

/**
 * This class represent the board of
 * all playable cards.
 * The cards in this board are arranged in grids.
 * 
 * @author Jonathan Christopher Jakub
 */
public class Board extends JPanel {

    /**
     * The game panel containing this board.
     */
    private GamePanel gamePanel;

    /**
     * The list of cards contained by this board.
     */
    private ArrayList<Card> cards = new ArrayList<Card>();

    /**
     * The list of image path for the cards.
     */
    private ArrayList<String> listOfFiles = new ArrayList<String>();

    /**
     * Constructor of the Board.
     * 
     * @param gamePanel the game panel that contains this board 
     * that connects the board with the control buttons.
     * @param imagePath the string of the folder path containing images for the cards.
     * 
     */
    public Board(GamePanel gamePanel, String imagePath){
        this.gamePanel = gamePanel;
        this.setMinimumSize(new Dimension(800, 1200));
        File[] files;
        try {
            files = new File(imagePath + "cards\\").listFiles();
            if (files.length == 0) {
                throw new IOException();
            }
            for (File file : files) {
                if (file != null && file.getName().toLowerCase().endsWith(".jpg")) {
                    this.listOfFiles.add(file.getCanonicalPath());
                }
            }
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(this.gamePanel, "File Not Found : Cards' images not found");
            System.exit(0);
        }

        this.listOfFiles.addAll(this.listOfFiles);
        Collections.shuffle(this.listOfFiles);
        
        int row = Board.determineSize(this.listOfFiles);
        int col = listOfFiles.size() / row;
        this.setLayout(new GridLayout(row,col));

        for(String s: this.listOfFiles) {
            Card newCard = new Card(imagePath + "zoo.jpg", s);
            newCard.setBackground(Color.WHITE);
            newCard.addActionListener(new CardListener(this.gamePanel));
            newCard.close();
            this.cards.add(newCard);
            this.add(newCard);
        }
    }

    /**
     * Helper method to the determine the number of row of the board.
     * Optimally, it will try to make the board with same number of rows and columns
     * (square root of the number of the cards).
     * Case it fails to do that, it will set the row with nearest divisible number to
     * the square root of the number of the cards.
     * 
     * @param listOfFiles the array list containing the string of the images path.
     */
    private static int determineSize(ArrayList<String> listOfFiles){
        for (int i = (int)(Math.sqrt(listOfFiles.size())); i > 1 ; i--){
            if (listOfFiles.size() % i == 0){
                return i;
            }
        }
        return 1;
    }

    /**
     * Reset the board and reshuffle the cards inside.
     */
    public void reset(){
        Collections.shuffle(this.listOfFiles);
        for(int i=0; i < this.cards.size() ; i++){
            this.cards.get(i).reset(this.listOfFiles.get(i));
        }
    }

    /**
     * Helper method to check if the player
     * already win (all cards matched).
     * 
     * @return boolean true if all cards matched, false otherwise.
     */
    public boolean checkWin() {
        int matched = 0;
        for(Card c : this.cards) {
            if(c.isMatched()) {
                matched++;
            }
        }
        if(matched == this.cards.size()) {
            return true;
        } else {
            return false;
        }
    }

}