import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import gui.view.GameFrame;

/**
 * This is the main class of the Memory Game
 * with sole purpose to run the game.
 * 
 * @author Jonathan Christopher Jakub
 */
public class Game {
    /**
     * This is the main method that runs the Game.
     * 
     * @param args unused.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                GameFrame game = new GameFrame("gui\\images\\");
                game.setVisible(true);
            }
        });
    }
}