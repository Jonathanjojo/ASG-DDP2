import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {
    static final double THRESHOLD = 250; // in kilograms
    static ArrayList<WildCat> Cats = new ArrayList<WildCat>();
    static ArrayList<TrainCar> Track = new ArrayList<TrainCar>();
    static DecimalFormat df = new DecimalFormat("#.00");

    //Main method
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        input.nextLine();
        //Listing the cats and instanciate them
        for (int i = 0; i < n; i++) {
            String[] catSpecs = input.nextLine().split(",");
            Cats.add(new WildCat(catSpecs[0],
                Double.parseDouble(catSpecs[1]),
                Double.parseDouble(catSpecs[2])));
        }
        TrainCar lastCar = null;
        int catsTransfered = 0;
        //Listing the cars and mounting to track while the total weight is just over THRESHOLD
        //Repeat until all cats are transfered
        while (catsTransfered < Cats.size()) {
            for (int i = catsTransfered; i < Cats.size(); i++) {
                if (i == catsTransfered) { //first cat on the track
                    TrainCar newCar = new TrainCar(Cats.get(i));
                    Track.add(newCar);
                    lastCar = newCar;
                } else {
                    TrainCar newCar = new TrainCar(Cats.get(i), lastCar);
                    Track.add(0, newCar);
                    lastCar = newCar;
                }
                if (Track.get(0).computeTotalWeight() > THRESHOLD) {
                    break;
                }
            }
            catsTransfered += Track.size();
            //Output
            System.out.println("The train departs to Javari Park");
            System.out.print("[LOCO]<--");
            Track.get(0).printCar();
            double avgMassIndex = Track.get(0).computeTotalMassIndex() / Track.size();
            System.out.println("Average mass index of all cats: "
                + df.format(avgMassIndex));
            String category;
            if (avgMassIndex < 18.5) {
                category = "*underweight*";
            } else if (avgMassIndex < 25) {
                category = "*normal*";
            } else if (avgMassIndex < 30) {
                category = "*overweight*";
            } else {
                category = "*obese*";
            }
            System.out.println("In average, the cats in the train are " + category);
            //Emptying track and filling with new cars and cats (if exist)
            Track.removeAll(Track);
        }
    }
}