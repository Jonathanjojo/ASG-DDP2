public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    //Constructor 1
    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.next = null;
    }

    //Constructor 2
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    //Compute Total Weight of this and following cars, return double
    public double computeTotalWeight() {
        double totalWeight = 0;
        if (next == null) {
            return (TrainCar.EMPTY_WEIGHT + this.cat.weight);
        } else {
            return totalWeight + ((TrainCar.EMPTY_WEIGHT + this.cat.weight) 
                + (this.next.computeTotalWeight()));
        }
    }

    //Compute Total Weight Mass Index this and following cars, return double
    public double computeTotalMassIndex() {
        double totalMassIndex = 0;
        if (this.next == null) {
            return this.cat.computeMassIndex();
        } else {
            return (double)(totalMassIndex + ((this.cat).computeMassIndex() 
                + ((this.next).computeTotalMassIndex())));
        }
    }

    //Print the car in order to console
    public void printCar() {
        if (this.next == null) {
            System.out.println("(" + (this.cat.name) + ")");
        } else {
            System.out.print("(" + (this.cat.name) + ")--");
            this.next.printCar();
        }
    }
}