public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    //Constructor, param: name,weight,length
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    //Compute Mass Index of this cat, param: - , return double
    public double computeMassIndex() {
        return (double)(10000 * this.weight / ((this.length) * (this.length)));
    }
}