package cage;

import animal.Animal;

/**
* Class : Cage.
* this is a concept class of the cage containing an animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/

public class Cage {
    private Animal animal;
    private String size;
    private char section;
    private String location;
    private int level;

    /**
     * This method is the Cage Class constructor.
     * @param animal Animal contained by this cage.
     * @param level level of this cage.
     */
    public Cage(Animal animal, int level) {
        this.animal = animal;
        this.level = level;
        this.detCage(animal);
    }

    /**
     * This method is to determine the size, location, and the section of this cage.
     * @param animal Animal contained by this cage.
     */
    private void detCage(Animal animal) {
        if (animal.getLocation().equals("indoor")) {
            if (animal.getLength() < 45) {
                this.size = "60cm x 60cm";
                this.location = "indoor";
                this.section =  'A';
            } else if (animal.getLength() <= 60) {
                this.size = "60cm x 90cm";
                this.location = "indoor";
                this.section =  'B';
            } else {
                this.size = "60cm x 120cm";
                this.location = "indoor";
                this.section =  'C';
            }
        } else {
            if (animal.getLength() < 75) {
                this.size = "120cm x 120cm";
                this.location = "outdoor";
                this.section =  'A';
            } else if (animal.getLength() <= 90) {
                this.size = "120cm x 150cm";
                this.location = "outdoor";
                this.section =  'B';
            } else {
                this.size = "120cm x 180cm";
                this.location = "outdoor";
                this.section =  'C';
            }   
        }
    }

    /**
     * Setters and Getters.
     */
    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return this.level;
    }

    public String getLocation() {
        return this.location;
    }

    public char getSection() {
        return this.section;
    }
}