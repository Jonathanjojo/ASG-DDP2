package animal.lion;

import animal.Animal;
import cage.Cage;
import java.util.ArrayList;
import sound.Sound;

/**
* Class : Lion.
* this is a subclass Lion of superclass Animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Lion extends Animal {
    private static final String species = "lion";
    private static final String location = "outdoor";
    private Sound[] sounds = {
        new Sound("See it hunting", "err...!", "Lion is hunting..\n"),
        new Sound("Brush the mane", "Hauhhmm!", "Clean the lion's mane..\n"),
        new Sound("Disturb it", "HAUHHMM!", "")
    };
    private Cage cage;

    /**
     * This method is the Lion Class constructor, it will make an object of Animal as well.
     * @param name string of the name of the lion.
     * @param bodyLength integers of length of the lion.
     */
    public Lion(String name, int bodyLength) {
        super(name, bodyLength, location, species);
        super.setSounds(sounds);
    }

}