package animal.parrot;

import animal.Animal;
import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;
import sound.Sound;

/**
* Class : Parrot.
* this is a subclass Parrot of superclass Animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Parrot extends Animal {
    private static final String species = "parrot";
    private static final String location = "indoor";
    private Sound[] sounds = {
        new Sound("Order to fly", "FLYYYY.....", "Parrot " + this.getName() + " flies!\n"),
        new Sound("Do conversation", "", ""),
        new Sound("", "HM?", "")
    };
    private Cage cage;

    /**
     * This method is the Parrot Class constructor, it will make an object of Animal as well.
     * @param name string of the name of the parrot.
     * @param bodyLength integers of length of the parrot.
     */
    public Parrot(String name, int bodyLength) {
        super(name, bodyLength, location, species);
        super.setSounds(this.sounds);
    }

    /**
     * This method will override the same method from the superclass: Animal.
     * It will print out the sound of the parrot.
     * This method produce unique output compared to the superclass' method (mimicking).
     * @param action as the generator of the sound that will be made.
     */
    public void makeSound(String action) {
        Scanner input = new Scanner(System.in);
        if (action.equals("Do conversation")) {
            System.out.print("You say: ");
            String words = input.nextLine();
            System.out.println(this.getName() + " says: " + words.toUpperCase());
            System.out.println("Back to office!");
        } else if (action.equals("")) {
            System.out.println(this.getName() + " says: HM?");
            System.out.println("Back to office!");
        } else {
            super.makeSound(action);
        }
    }

}