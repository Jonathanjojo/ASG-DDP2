package animal.eagle;

import animal.Animal;
import cage.Cage;
import java.util.ArrayList;
import java.util.Random;
import sound.Sound;

/**
* Class : Eagle.
* this is a subclass Eagle of superclass Animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Eagle extends Animal {
    private static final String species = "eagle";
    private static final String location = "outdoor";
    private Sound[] sounds = {
        new Sound("Order to fly", "kwaakk...", "")
    };
    private Cage cage;

    /**
     * This method is the Eagle Class constructor, it will make an object of Animal as well.
     * @param name string of the name of the eagle.
     * @param bodyLength integers of length of the eagle.
     */
    public Eagle(String name, int bodyLength) {
        super(name, bodyLength, location, species);
        super.setSounds(sounds);
    }

    /**
     * This method will override the same method from the superclass: Animal.
     * It will print out the sound of the eagle and the description.
     * This method produce unique output compared to the superclass' method (hurt).
     * @param action as the generator of the sound that will be made.
     */
    public void makeSound(String action) {
        if (!action.equals("")) {
            System.out.print(this.getSounds()[0].getDesc());
            System.out.println(this.getName() 
                + " makes a voice: " 
                + this.getSounds()[0].getSound());
            System.out.println("You hurt!\n");
        } else {
            System.out.println("You do nothing...");
        }
        System.out.println("Back to office!");
    }
    
}