package animal.hamster;

import animal.Animal;
import cage.Cage;
import java.util.ArrayList;
import sound.Sound;

/**
* Class : Hamster.
* this is a subclass Hamster of superclass Animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Hamster extends Animal {
    private static final String species = "hamster";
    private static final String location = "indoor";
    private Sound[] sounds = {
        new Sound("See it gnawing", "ngkkrit.. ngkkrrriiit", ""),
        new Sound("Order to run in the hamster wheel", "trrr....trrr....", "")
    };
    private Cage cage;

    /**
     * This method is the Hamster Class constructor, it will make an object of Animal as well.
     * @param name string of the name of the hamster.
     * @param bodyLength integers of length of the hamster.
     */
    public Hamster(String name, int bodyLength) {
        super(name, bodyLength, location, species);
        super.setSounds(sounds);
    }

}