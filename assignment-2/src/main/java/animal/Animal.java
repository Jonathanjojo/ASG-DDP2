package animal;

import animal.Animal;
import cage.Cage;
import java.util.Random;
import sound.Sound;

/**
* SuperClass : Animal.
* parent of : Cat, Eagle, Hamster, Lion, Parrot.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/

public class Animal {
    private String name;
    private int bodyLength;
    private Cage cage;
    private Sound[] sounds;
    private String location;
    private String species;

    /**
     * This method is the Animal Class constructor.
     * @param name string of the name of the animal.
     * @param bodyLength integers of length of the animal.
     * @param species string of the species of the animal.
     */
    public Animal(String name, int bodyLength, String location, String species) {
        this.name = name;
        this.bodyLength = bodyLength;
        this.location = location;
        this.species = species;
    }

    /**
     * This method is used to print out the sound of the animals.
     * @param action as the generator of the sound that will be made.
     */
    public void makeSound(String action) {
        if (!action.equals("")) {
            int i;
            do {
                i = new Random().nextInt(this.getSounds().length);
            } while (!this.getSounds()[i].getAction().equals(action));

            System.out.print(this.getSounds()[i].getDesc());
            System.out.println(this.getName() 
                + " makes a voice: " 
                + this.getSounds()[i].getSound());
        } else {
            System.out.println("You do nothing...");
        }
        System.out.println("Back to office!\n");
    }

    /**
     * Setters and Getters.
     */
    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.bodyLength;
    }

    public String getSpecies() {
        return this.species;
    }

    public void setCage(Cage cage) {
        this.cage = cage;
    }

    public Cage getCage() {
        return this.cage;
    }

    public Sound[] getSounds() {
        return this.sounds;
    }

    public void setSounds(Sound[] sounds) {
        this.sounds = sounds;
    }

    public String getLocation() {
        return this.location;
    }

}