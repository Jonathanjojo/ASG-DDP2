package animal.cat;

import animal.Animal;
import cage.Cage;
import java.util.ArrayList;
import sound.Sound;

/**
* Class : Cat.
* this is a subclass Cat of superclass Animal.
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Cat extends Animal {
    private static final String species = "cat";
    private static final String location = "indoor";
    private Sound[] sounds = {
        new Sound("Brush the fur", "Nyaaan....", "Time to clean " + this.getName() + "'s fur\n"),
        new Sound("Cuddle", "Miaaaw..", ""),
        new Sound("Cuddle", "Purrr..", ""),
        new Sound("Cuddle", "Mwaw!", ""),
        new Sound("Cuddle", "Mraaawr!", "")
    };
    private Cage cage;

    /**
     * This method is the Cat Class constructor, it will make an object of Animal as well.
     * @param name string of the name of the cat.
     * @param bodyLength integers of length of the cat.
     */
    public Cat(String name, int bodyLength) {
        super(name, bodyLength, location, species);
        super.setSounds(this.sounds);
    }

}