import animal.Animal;
import animal.cat.Cat;
import animal.eagle.Eagle;
import animal.hamster.Hamster;
import animal.lion.Lion;
import animal.parrot.Parrot;
import cage.Cage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import sound.Sound;
import zoo.Zoo;

/**
* Class : Main.
* defined main() in this class
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/

public class Main {
    private static ArrayList<? extends Animal> Cats = new ArrayList<Cat>();
    private static ArrayList<? extends Animal> Lions = new ArrayList<Lion>();
    private static ArrayList<? extends Animal> Hamsters = new ArrayList<Hamster>();
    private static ArrayList<? extends Animal> Parrots = new ArrayList<Parrot>();
    private static ArrayList<? extends Animal> Eagles = new ArrayList<Eagle>();
    private static Scanner input = new Scanner(System.in);

    /**
    * This is the main method which makes use of the classes: Animal, Cage, Sound.
    * @param args Unused.
    */
    public static void main(String[] args) {
        // Input animals
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        
        Cats = Zoo.inputAnimals("cat");
        Lions = Zoo.inputAnimals("lion");
        Eagles = Zoo.inputAnimals("eagle");
        Parrots = Zoo.inputAnimals("parrot");
        Hamsters = Zoo.inputAnimals("hamster");

        System.out.println("Animals has been successfully recorded!");
        System.out.println("=============================================");

        //Arrange and Rearrange
        System.out.println("Cage arrangement:");
        Zoo.arrange(Cats);
        Zoo.rearrange(Cats);

        Zoo.arrange(Lions);
        Zoo.rearrange(Lions);

        Zoo.arrange(Eagles);
        Zoo.rearrange(Eagles);

        Zoo.arrange(Parrots);
        Zoo.rearrange(Parrots);

        Zoo.arrange(Hamsters);
        Zoo.rearrange(Hamsters);

        //Totaling
        System.out.println("\nANIMALS NUMBER:");
        System.out.println("cat: " + Cats.size());
        System.out.println("lion: " + Lions.size());
        System.out.println("parrot: " + Parrots.size());
        System.out.println("eagle: " + Eagles.size());
        System.out.println("hamster: " + Hamsters.size());

        System.out.println("");
        System.out.println("=============================================");

        //Visiting
        while (true) {
            try {
                int c;
                while (true) {
                    try {
                        System.out.println("Which animal you want to visit?");
                        System.out.println(
                            "(1:Cat, 2:Eagle, 3:Hamster, 4:Parrot, 5:Lion, 99:exit)");
                        c = Integer.parseInt(input.next());
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Please choose a valid option");
                    }
                }
                input.nextLine();
                if (c == 99) {
                    System.out.println("Exiting Javari park..");
                    break;
                } else {
                    if (c == 1) {
                        if (Cats.size() > 0) {
                            System.out.print("Mention the cat you want to visit: ");
                            String name = input.nextLine();
                            Zoo.visit(Cats, name);
                        } else {
                            System.out.println("Sorry, there are no cats in this park\n");
                        }
                    } else if (c == 2) {
                        if (Eagles.size() > 0) {
                            System.out.print("Mention the eagle you want to visit: ");
                            String name = input.nextLine();
                            Zoo.visit(Eagles, name);
                        } else {
                            System.out.println("Sorry, there are no eagles in this park\n");
                        }
    
                    } else if (c == 3) {
                        if (Hamsters.size() > 0) {
                            System.out.print("Mention the hamster you want to visit: ");
                            String name = input.nextLine();
                            Zoo.visit(Hamsters, name);
                        } else {
                            System.out.println("Sorry, there are no hamsters in this park\n");
                        }
    
                    } else if (c == 4) {
                        if (Parrots.size() > 0) {
                            System.out.print("Mention the parrot you want to visit: ");
                            String name = input.nextLine();
                            Zoo.visit(Parrots, name);
                        } else {
                            System.out.println("Sorry, there are no parrots in this park\n");
                        }
    
                    } else if (c == 5) {
                        if (Lions.size() > 0) {
                            System.out.print("Mention the lion you want to visit: ");
                            String name = input.nextLine();
                            Zoo.visit(Lions, name);
                        } else {
                            System.out.println("Sorry, there are no lions in this park\n");
                        }
                    } else {
                        System.out.println("Please choose a valid option");
                    }
                }
            } catch (NullPointerException e) {
                System.out.println("Please choose a valid option");
            }
            
        }
    }
}