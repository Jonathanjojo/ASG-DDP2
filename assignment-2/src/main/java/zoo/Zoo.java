package zoo;

import animal.Animal;
import animal.cat.Cat;
import animal.eagle.Eagle;
import animal.hamster.Hamster;
import animal.lion.Lion;
import animal.parrot.Parrot;
import cage.Cage;
import java.lang.NumberFormatException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import sound.Sound;

/**
* Class : Zoo.
* Acts as a caretaker of the zoo : helper class of Main
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/
public class Zoo {
    private static Scanner input = new Scanner(System.in);

    /**
     * This method is used to record the animals input into coresponding arraylists.
     * @param species string of the name of the species of the to be input animal.
     * @return ArrayList of type ? extends Animal.
     */
    public static ArrayList<? extends Animal> inputAnimals(String species) {
        int n;
        while (true) {
            try {
                System.out.print(species + ": ");
                n = Integer.parseInt(input.next());
                if (n < 0) {
                    throw new NumberFormatException("");
                }
                break;
            } catch (java.lang.NumberFormatException e) {
                System.out.println("Invalid input");
            }
        }
        input.nextLine();
        while (true) {
            try {
                if (n != 0) {
                    while (true) {
                        try {
                            System.out.println("Provide information of " + species + "(s): ");
                            String[] inputArray = input.nextLine().split("\\,");
                            String name;
                            int length;
                            if (species.equals("cat")) {
                                ArrayList<Cat> animal = new ArrayList<Cat>();
                                for (int i = 0; i < n; i++) {
                                    name = inputArray[i].split("\\|")[0];
                                    length = Integer.parseInt(inputArray[i].split("\\|")[1]);
                                    if (length > 0) {
                                        animal.add(new Cat(name, length));
                                    } else {
                                        throw new InputMismatchException("");
                                    }
                                }
                                return animal;
                            } else if (species.equals("lion")) {
                                ArrayList<Lion> animal = new ArrayList<Lion>();
                                for (int i = 0; i < n; i++) {
                                    name = inputArray[i].split("\\|")[0];
                                    length = Integer.parseInt(inputArray[i].split("\\|")[1]);
                                    if (length > 0) {
                                        animal.add(new Lion(name, length));
                                    } else {
                                        throw new InputMismatchException("");
                                    }
                                }
                                return animal;
                            } else if (species.equals("hamster")) {
                                ArrayList<Hamster> animal = new ArrayList<Hamster>();
                                for (int i = 0; i < n; i++) {
                                    name = inputArray[i].split("\\|")[0];
                                    length = Integer.parseInt(inputArray[i].split("\\|")[1]);
                                    if (length > 0) {
                                        animal.add(new Hamster(name, length));
                                    } else {
                                        throw new InputMismatchException("");
                                    }                        
                                }
                                return animal;
                            } else if (species.equals("eagle")) {
                                ArrayList<Eagle> animal = new ArrayList<Eagle>();
                                for (int i = 0; i < n; i++) {
                                    name = inputArray[i].split("\\|")[0];
                                    length = Integer.parseInt(inputArray[i].split("\\|")[1]);
                                    if (length > 0) {
                                        animal.add(new Eagle(name, length));
                                    } else {
                                        throw new InputMismatchException("");
                                    }                        
                                }
                                return animal;
                            } else {
                                ArrayList<Parrot> animal = new ArrayList<Parrot>();
                                for (int i = 0; i < n; i++) {
                                    name = inputArray[i].split("\\|")[0];
                                    length = Integer.parseInt(inputArray[i].split("\\|")[1]);
                                    if (length > 0) {
                                        animal.add(new Parrot(name, length));
                                    } else {
                                        throw new InputMismatchException("");
                                    }                        
                                }
                                return animal;
                            }
                        } catch (NumberFormatException | InputMismatchException e) {
                            System.out.println("Input valid integers");
                        }
                    }
                    
                } else {
                    if (species.equals("cat")) {
                        return new ArrayList<Cat>();
                    } else if (species.equals("lion")) {
                        return new ArrayList<Lion>();
                    } else if (species.equals("hamster")) {
                        return new ArrayList<Hamster>();
                    } else if (species.equals("eagle")) {
                        return new ArrayList<Eagle>();
                    } else {
                        return new ArrayList<Parrot>();
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Invalid format");
            }
        }  
    }

    /**
     * This method is used to visit the animals available.
     * each visited animals would make certain sounds if treated with certain actions.
     * @param animal arraylist containing the animals that are going to be visited.
     * @param name string name of the going to be visited animal.
     * @exception NullPointerException On unavailable choices (caught).
     */
    public static void visit(ArrayList<? extends Animal> animal, String name) {
        boolean found = false;
        for (int j = 0; j < animal.size(); j++) {
            if (name.equals(animal.get(j).getName())) {
                found = true;
                System.out.println("You are visiting " + name 
                    + " (" + animal.get(0).getSpecies() + ") now, what would you like to do?");
                HashMap<Integer, String> actions = new HashMap<Integer, String>();
                int counter = 1;
                for (int i = 0; i < animal.get(j).getSounds().length; i++) {
                    if (!actions.containsValue(animal.get(j).getSounds()[i].getAction())) {
                        actions.put(counter, animal.get(j).getSounds()[i].getAction());
                        if (!animal.get(j).getSounds()[i].getAction().equals("")) {
                            System.out.print(counter + ": " 
                                + animal.get(j).getSounds()[i].getAction() + " ");
                            counter++;
                        }
                    }
                }
                System.out.println();
                try {
                    try {
                        animal.get(j).makeSound(actions.get(Integer.parseInt(input.next())));
                    } catch (NumberFormatException e) {
                        System.out.println("Please choose a valid option");
                    }
                } catch (NullPointerException e) {
                    animal.get(j).makeSound("");
                }
                break;
            }
        }
        if (found == false) {
            System.out.println("there is no " + animal.get(0).getSpecies() 
                + " with that name! Back to the office!");
        }
    }

    /**
     * This method is used to arrange the cages of the animals into several levels (1-3).
     * @param animal arraylist containing the animals whose cages are going to be arranged.
     */
    public static void arrange(ArrayList<? extends Animal> animal) {
        if (animal.size() > 0) {
            int[] arrangement = division(animal.size());
            System.out.println("location: " + animal.get(0).getLocation());
            for (int level = 3; level > 0; level--) {
                System.out.print("level " + level + ":");
                for (int i = arrangement[level - 1]; i < arrangement[level]; i++) {
                    animal.get(i).setCage(new Cage(animal.get(i), level));
                    System.out.print(animal.get(i).getName() 
                        + " (" + animal.get(i).getLength() 
                        + " - " + animal.get(i).getCage().getSection() + "), ");
                }
                System.out.println();
            }
            System.out.println();
        } 
    }

    /**
     * This method is used to rearrange the cages: reversing the levels order (1-3).
     * @param animal arraylist containing the animals whose cages are going to be rearranged.
     */
    public static void rearrange(ArrayList<? extends Animal> animal) {
        if (animal.size() > 0) {
            System.out.println("After rearrangement... ");
            for (int i = 0; i < animal.size(); i++) {
                int newLevel = animal.get(i).getCage().getLevel() == 3 
                    ? 1 : animal.get(i).getCage().getLevel() + 1;
                animal.get(i).getCage().setLevel(newLevel);
            }
            for (int level = 3; level > 0; level--) {
                System.out.print("level " + level + ":");
                for (int i = animal.size() - 1; i >= 0; i--) {
                    if (animal.get(i).getCage().getLevel() == level) {
                        System.out.print(
                            animal.get(i).getName() 
                            + " (" + animal.get(i).getLength() 
                            + " - " + animal.get(i).getCage().getSection() + "), ");
                    }
                }
                System.out.println();
            }
            System.out.println();
        } 
    }
    
    /**
     * This is the helper method for arranging the animal cages.
     * It will divide the number of animals into 3 levels evenly, starting from the bottom level(1).
     * @param n integer of the number of animals.
     * @return int[] of arranged indexes with number of animals per level as differences.
     */
    public static int[] division(int n) {
        int left = n;
        int first = (n >= 3) ? (int) Math.ceil((float) left / 3) : 0;
        left = (n - first) < 0 ? 0 : (n - first);
        int second = (n >= 2) ? (int) Math.ceil((float)(left) / 2) : 0;
        left = (left - second) < 0 ? 0 : (left - second);
        int third = left;
        int[] result = {
            0,
            third,
            third + second,
            first + second + third
        };
        return result;
    }

}