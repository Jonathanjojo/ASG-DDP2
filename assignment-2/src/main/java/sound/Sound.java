package sound;

/**
* Class : Sound.
* this is a concept class of the sound made by animals. 
* (each sound has unique actions and descriptions).
* @author  Jonathan Christopher Jakub.
* @since   2-4-2018.
*/

public class Sound {

    String action;
    String sound;
    String desc;

    /**
     * This method is the Sound Class constructor.
     * @param action string of the action done to the animal.
     * @param sound string of the sound made.
     * @param desc string of the description of the sound and the action.
     */
    public Sound(String action, String sound, String desc) {
        this.action = action;
        this.sound = sound;
        this.desc = desc;
    }

    /**
     * Setters and Getters.
     */
    public String getAction() {
        return this.action;
    }

    public String getSound() {
        return this.sound;
    }

    public String getDesc() {
        return this.desc;
    }

}